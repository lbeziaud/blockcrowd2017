\section{Background}%
\label{sec:Background}
We start by presenting the motivation behind the \emph{blockchain}
technology, and a high-level view of its implementation. We then give 
a brief description of two systems used by the blockchain protocol to
deter service abuses, \emph{proof-of-work} and \emph{proof-of-stake},
along with the required basic principles on \emph{digital signatures}.


\subsection{Traffic Monitoring}
The goal of traffic monitoring is to provide near real-time
information on road usage, that is being able to estimate the traffic
flow of a given road at a given time. The system must provide current
road usage, along with meaningful statistics on past traffic
information. This last point motivates our usage of the blockchain
technology, as it provides a reliable historic of application data.

\subsection{Blockchain}
We are interested in sharing data on a distributed network because it
offers several properties that would go well with such a
system. Besides the defining property of sharing work and power of
decision among peers, the fact that such a network can be asynchronous
fits our needs, because delays in communication will certainly occur
in a large system where all users may not always have reliable access
to the network (for example people traveling in the countryside). We
would also want our system to be permissionless, ie.\ for users to be
able to go in and out of the system at will, because that is exactly
how real automobile networks behave. That is why we will base our
implementation on a blockchain protocol, a type of distributed
algorithm that verifies these wanted properties and was introduced by
Nakamoto in 2008~\cite{NakamotoBitcoin2008} as the foundation of the
now widely used Bitcoin currency.

The purpose of a blockchain is to allow the users of an asynchronous
distributed network to share a common history on the events that 
impact the state of the system, in our case the presence of a traffic
jam or not, even when users may
join or quit the network at any moment without the need for
permission. A blockchain is a protocol in which each user possess a
variable \emph{state} containing \emph{blocks}. Blocks themselves
contains a set of \emph{messages} and a header pointing at the most
recently created block before them. The blocks thus form a
chronologically ordered chain, hence the name ``blockchain''.  We
want to achieve a \emph{public} ledger, ie.\ a common set of blocks
that verifies the properties of persistency (once a message gets added
to the ledger, it is never removed) and liveliness (if enough honest
players want a message to be added to the ledger, it will eventually
be added).  Such a ledger should be strongly resistant to common
attacks such as sybil (when an attacker spawns lots of new players, in
this case to control the messages added to the ledger) and allow each
user to trust the data it contains.  In 2016,
Pass~\cite{PassAnalysis2016} proved that a blockchain that possesses
the properties of \emph{consistency} (the fact that all users share
an identical chain of a growing number of blocks), \emph{future
  self-consistence} (the fact that this common chain does not change
as time goes on), \emph{chain-growth} (the fact an honest player's
chain will grow with as time goes on) and \emph{chain quality} (the
fact that honest players actually get their messages added into the
the chain) will allow for such a ledger.

Path also proved that the Nakamoto protocol~\cite{NakamotoBitcoin2008}
indeed possessed these properties.  In this protocol, there are three
types of nodes in the network: \emph{players}, who create messages and
broadcasts them to the network; \emph{miners} who build blocks by
collecting the messages they receive (once a block is mined, this
block is broadcast within the network); \emph{users} who add the
blocks they receive into the blockchain if and only if all the
messages they contain are valid.

To ensure that at least one block is already shared by at least all
the users and miners, the first block of the chain, called genesis
block, which purpose is solely to be said first block, is directly
encoded into the protocol, so that every node that uses it will
automatically accept the genesis block. As for subsequent blocks, it
is definitely possible for two different blocks to be accepted by two
different users, thus creating a fork that \emph{de facto} transforms
the chain into a tree.

\begin{figure}
  \centering
  \begin{forest}
    [\(B_0\), fill={red!20}, for tree={edge={<-},grow=east}
    [\(B_1\), for tree={fill=green!20}
    [\(B'_2\), for tree={fill=gray!20}]
    [\(B_2\)
    [\(B_3\)] ] ] ]
  \end{forest}
  \caption{blockchain example (with a fork)}%
  \label{fig:blockchain}
\end{figure}

In order to preserve the unicity of the ledger shared by all users, a
branch must be chosen and one user must accept the other block and
discard his own. To solve this problem, a user will, upon receiving a
block, check if it represents a chain longer than the one represented
by his last accepted block. If so, he will discard this block and
accept the new one. If not, he will discard the new block. In the
former case, if there exists messages that are present in the
discarded block but not in the newly accepted chain, he will broadcast
them to the system for the miners to add them into the block they will
start working on. This method allows for the
consistency property to be respected.  Now, one last issue is the need
for users to add their block into the blockchain in a desynchronised
fashion. Otherwise the forks created by the addition of blocks from
different users would never be solved.  This is the goal of the
proof-of-work scheme.

\subsection{Proof-of-work}
A proof-of-work~\cite{DworkPricing1993,JakobssonProofs1999} (POW) is a
computational puzzle used to deter denial of service
attacks~\cite{BorisovComputational2006} and other service abuse by
requiring some work from the service requester. It is a piece of data
which is expensive (costly, time-consuming) to produce but efficiently
verifiable by others and which satisfies certain requirements. It was
first introduced as cost-functions in~\cite{DworkPricing1993}.

Common proof-of-work implementations use a hash function as a building
block, as cryptographic hashes are hard to invert. This is the case of
Hashcash~\cite{BackHashcash2002}, used as the
Bitcoin~\cite{NakamotoBitcoin2008} mining function, which is using a
double SHA256 function.

The proof-of-work relies on energy use, since it requires high
computation power. Alternatives to POW have been proposed to mitigate
this issue, such as the proof-of-stake (POS). The main idea behind POS
is that instead of using its capital to buy computers and electricity
to compute POW tokens, a user use its capital to acquire the
tokens. In cryptocurrencies such as
Peercoin~\cite{VasinBlackcoin2014}, a proof-of-stake asks users to
prove ownership of a certain amount of currency (their ``stake'' in
the currency).

\subsection{Proof-of-stake}
The main consideration regarding the PoW is the energy required to
produce hashing operation~\cite{odwyer2014footprint}. At the time of
this writing $2^{51}$ hash are generated by the network to create a
single block. Many discussions on the bitcoin forum led to a mechanism
based on ``proof-of-stake'' (PoS)~\cite{quantummechanic2011stake} to
obtain a more energy efficient process. Instead of using computational
ressources, the miners randomly elect one of them based of the amount
of stake reported in the ledger that each of them possesses. This
protocol creates a self-referential blockchain relying on the
stakeholders to assign work to themselves and has been formally
studied by Bentov~\cite{Bentov2016}. This process does not
require computational power from the miners, but do suffer from some
security issues~\cite{Bentov2016}.

\subsection{Digital Signature}
A digital signature is a mathematical scheme employing asymmetric
cryptography for demonstrating the authenticity of digital
messages. Although digital signatures are not the focus of this paper,
it is deeply used by the blockchain protocol. We only provide a brief
overview; the interested reader is referred to~\cite{aki1983digital}.
A digital signature scheme consists of three algorithms:
\begin{itemize}
  \item A key \emph{generation} algorithm that selects a private key
  uniformly at random from a set of possible private keys. The
  algorithm outputs the private key and a corresponding public key.
  \item A \emph{signing} algorithm that, given a message and a private key, produces a signature.
  \item A signature \emph{verifying} algorithm that, given the
  message, public key and signature, either accepts or rejects the
  message's claim to authenticity.
\end{itemize}

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-master: "main.tex"
%%% ispell-local-dictionary: "english"
%%% End:

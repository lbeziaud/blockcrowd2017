\section{Model}%
\label{sec:Model}

In this section we define the model under which our framework
operates. We describe the entities involved, the infrastructure
assumptions, and the security model that is managed.

\subsection{Entities}
There are four entities interacting in our framework.

\textbf{Sensors} are mobile nodes who want to transmit their
local observations to the system. They correspond to the cars in our
framework.

\textbf{Aggregators} are geographically distributed to aggregate local
observations of sensors nodes. They possess limited calculation power
and memory capabilities.

\textbf{Miners} provide computing power. They are always
accessible. Filling stations play the role of miners.

The \textbf{clients} want to read the aggregated data of the
system.

\subsection{Infrastructure}

We make the assumption that sensors nodes always know their geographic
position, \emph{ie.} they have access to a GPS signal at all time,
without any sort of spoofing.
%
Furthermore, we assume a nearly full network coverage for the
sensor. While we allow for some of the messages sent by the sensors to
the aggregators to be lost, this must be in adequation with the
distribution of of honest vs malicious sensors.

\subsection{Security}

We assume that all entities are correct, \emph{ie.} they follow the
protocol. % removed 'honestly' to avoid contradiction
%
Sensors can however lie about the reported state, maliciously or due
to a hardware or software error. We require a strict majority
(\(> 50\%\)) of honest sensors.
%
Aggregators are trusted entities.
%
Miners can ignore transactions. We allow for miners collusion but we
assume that colliding nodes are known. We say that miners collusion
occurs when two filling stations belong to the same corporation.
%
We assume there exists a directory of aggregators and miners nodes,
along with a database of digital signatures used by these nodes. This
directory allows a sensor to identify its nearest aggregators nodes.
%
The privacy of observers is not addressed in this paper.

\section{Framework}%
\label{sec:Framework}

In this section we detail our framework, which is depicted in
Figure~\ref{fig:overview}.
%
Embedded sensors on cars (1) collect observations on the local traffic
state, which are (2) sent to local nodes. These nodes (3) pre-process
and aggregate data, before (4) sending it to a miner. The miner (5)
aggregates the collected reports into a `block'', before (6)
broadcasting the result.

The rest of this section gives the formal structure of our central
data structure-–a blockchain--before describing the algorithms run by
each entity.

\begin{figure}[ht]
  \includegraphics[width=\linewidth]{overview.png}
  \caption{Global view of the framework}%
  \label{fig:overview}
\end{figure}

\subsection{Blockchain Structure}

\begin{definition}[Transaction]%
  \label{Transaction}
A transaction \(t\) is a tuple such that
\[ t = (\sigma_a, pk_s, a)\]
with \(\sigma_a\) the signature over \(a\) with the private key \(pk_s\) of a sensor \(s\),
\(pk_s\) the public key of the sensor \(s\) and \(a\) an aggregate modelled as a
couple
  \[a = (r, st)\]
  with \(r\) a road and \(st\) its status.
\end{definition}

\begin{definition}[Block]%
  \label{Block}
  A block \(b_i\) is build by a miner \(m\) and is defined by
  \[b_i = (\sigma_h, pk_m, h)\]
  with \(\sigma_h\) the signature over \(h\) with the private key of \(m\),
  \(pk_m\) the public key of the miner \(m\) and \(h\) the header which is
  modeled as
  \[h = (Hash(b_{i-1}), T, time)\]
  with \(Hash(b_{i-1})\) the result of the relation \(Hash\) over the previous
  block \(b_{i-1}\), \(T\) a set of transactions and \(time\) the timestamp when
  the block \(b_i\) has been created by the miner \(m\).
\end{definition}

In our context, the miners do not own stake but a public key registered in a
decentralized database. The PoS mechanism we use in our framework is based on
the idea of a block containing the necessary information for the miners to know
who has to create the next block. The timestamp written in the header of the 
block is used as the seed of a, globally shared over the miners, random number 
uniformly generating function which draw the public key of the newly elected 
miner.

A single entity in our system which owns a significant percentage of the miners
can create a sequence of multiple blocks with high probability. To prevent the 
censorship that can be created by this collusion of miners, a more robust method
can be use to elect the next miner. The owner of the filling stations are 
detailed in the database, which gives the opportunity to draw uniformly into
the set of miners not owned by the previous miner's owner.

\begin{definition}[Proof-of-stake]%
  \label{ProofOfStake}

PoS is a relation defined as
\[PoS \rightarrow t \times pk_{b_i} \rightarrow PK \setminus PK_{b_i}\]
such that from a timestamp \(t\) and the public key of the previous miner 
\(pk_{b_i}\) draws uniformly over the set of miner's public key minus the set
of miners public keys owned by the owner of the previously elected miner.
\end{definition}

\subsection{Sensors}

\begin{definition}[Traffic state]%
  \label{Traffic state}
  A traffic state \(st\) is an integer which takes value into an 
  arbitrary range depending on the required accuracy of the system.
  The following use of traffic states will take values into \(\llbracket0,5\rrbracket\),
  to quantify the fluidity of the traffic.
\end{definition}

% Observation
An observation \(o\) is modeled as
\[o = (c, s, n, p)\]
with \(c\) the coordinates of the sensor, \(s\) the status of the road, \(n\) a 
randomly generated number and \(p\) a proof over the tuple \((c, s, n)\).

% Protocol
A car behave as a sensor in our system, a sensor which report road status. A
sensor can acknowledge its position with a GPS tracking unit and evaluate the
traffic status. We assume a trusted GPS tracking unit, so that the car
position cannot be falsified. Whenever a car enters a road it has the ability to
observe its fluidity and warn the system. Algorithm 1 describe the procedure to
generate such observation.

% Algorithms

\begin{algorithm}[ht]
  \caption{Sensor's procedure}
  Let $c$ be the car geographic position obtained from GPS. \\
  Let $s$ be the current observed road state. \\
  Compute the proof-of-work \((p,r) \gets POW(c, s)\) \\
  Build the observation $o \gets (c, s, r, p)$. \\
  Send observation $o$ to the nearest aggregators.
\end{algorithm}

% POW
A PoW is required from a sensor to generate a valid observation. The 
PoW is made over a target \(t\) which contains the coordinates of the
car along with the observed status. We assure unicity over 
observations by adding a randomly generated number to \(t\).

\begin{algorithm}
  \caption{Sensor's POW scheme}
  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}
  \SetKwFunction{Hash}{Hash}
  \SetKwFunction{Append}{Append}
  \SetKwRepeat{Do}{do}{while}
  \Input{car coordinates $c \in \mathbb{R}^2$, road state $s \in \{0, 1\}$} 
  \Output{proof-of-work $p \in \mathbb{N}$}
  Let $n \in \{0, 2^{64}\}$ be a random integer.\\
  Let $p \gets (c, s, n)$ of the payload.\\
  Let $H_p \gets Hash(p)$ be the hash of our payload.\\
  Let $d \in \{10^1,10^{10}\}$ be the difficulty.\\
  Let $t \gets 2^{64} \div d$ be the target.\\
  Let $N \gets 0$ be our nonce.\\
  Let $g$ be our guess.\\
  \Do{g > t}{
    $N \gets N + 1$\\
    $g \gets Hash(Hash(N + H_p))$
  }
  \Return{\Append{$g, r$}}
\end{algorithm}

% Adversarial Behavior
The difficulty of the proof depends on the mean time to traverse the road the
car is currently reporting about. The PoW removes the ability to overflow the
system to the sensors and this criteria importance will be described later as
well as the fact that a sensor can still generate false information.

\subsection{Aggregators}
% Algorithm
\begin{algorithm*}
  \caption{The aggregator's procedure executed upon receiving an observation from an observer.}
  %
  \SetKwBlock{Block}{}{}
  \SetKwHangingKw{Assert}{assert}
  \SetKwFunction{Hash}{Hash}
  \SetKwFunction{Sign}{Sign}
  \SetKwFunction{Majvote}{MajorityVoting}
  \SetKwArray{D}{D}
  \DontPrintSemicolon%
  %
  \Begin{
  Let $D$ a table associating for a road $r$ and a status $s$ a set of integers.
  Let \(pk_s\) the public key of the sensor.\\
  Let \(sk_s\) the secret key of the sensor.\\
  }
  \Block({\textbf{upon receiving} observation $(c, s, n, p)$ \textbf{from} \emph{an} observer}){
    \Begin(\emph{verify}){
      Let $o \gets (c, s, n)$. \\
      $Assert(Hash(o)) = p$
    }
    \Begin(\emph{update}){
      Let $r$ be the road index associated to the position $c$. \\
      Add $n$ to the road's status table \D{$r,s$}. \\
      \If(\tcc*[f]{ie.\ enough observation}){$\lvert$\D{$r$}$\rvert > \theta_r$}{
        Let $st \gets \Majvote(\D{r})$ the result of majority voting. \\
        $a \gets (r, st)$\\
        $\sigma\gets$ \Sign{$sk_s$, $a$} \\
        $t \gets (\sigma_a, pk_s, a)$ \\
        Send transaction $t$ to $m$ the elected miner.\\
        \D{$r$} $\gets \emptyset$
      }
    }
  }
\end{algorithm*}

% Protocol
An aggregator's role is to remove the most of the load over the network by
aggregating the observations made by the sensors. The aggregators are collecting
the observations made by the sensors from at least one road. Two aggregators do
not collect observations from the same road.
An aggregate is generated as soon as a required amount \(\theta_r\) of 
observations have been received for the road \(r\). The status reported in the 
transaction of the aggregate is computed by an arbritrary aggregation function 
(e.g. Max, Mean, $\dots$). When the transaction is finally created it is then
sent to the miner in charge of mining the next block.

\begin{figure}[ht]
\centering

  \begin{tikzpicture}[
    aggr/.style={thick, rounded corners, line width=3mm, line cap=round},
    road/.style={sloped, inner xsep=1pt, inner ysep=0pt, font=\small},
    aggrlab/.style={minimum height=3mm, inner xsep=2pt, inner ysep=0pt, font=\small},
    ]
    \coordinate (A) at (0,0);
    \coordinate (B) at (0,2);
    \coordinate (C) at (2,0);
    \coordinate (D) at (1,1);
    \coordinate (E) at (3,1);
    \coordinate (F) at (2,2);
    %
    \draw [aggr, draw=green!20]
    (E.center) -- (F.center)
    (E.center) -- (D.center)
    (E.center) -- (C.center);
    \draw [aggr, draw=blue!20]
    (B.center) -- (F.center)
    (D.center) -- (F.center);
    \draw [aggr, draw=red!20]
    (A.center) -- (B.center)
    (A.center) -- (C.center)
    (A.center) -- (D.center)
    (C.center) -- (D.center);
    %
    \node[aggrlab, fill=red!20, xshift=-0.3mm]
    at (A) [anchor=east] {aggregator \(a\)};
    \node[aggrlab, fill=blue!20, xshift=0.3mm]
    at (F) [anchor=west] {aggregator \(b\)};
    \node[aggrlab, fill=green!20, xshift=0.3mm]
    at (E) [anchor=west] {aggregator \(c\)};
    %
    \draw
    (A) -- node[road, fill=red!20] {\(a_1\)} (B)
    (A) -- node[road, fill=red!20] {\(a_2\)} (C)
    (A) -- node[road, fill=red!20] {\(a_3\)} (D)
    (C) -- node[road, fill=red!20] {\(a_4\)} (D);
    \draw
    (E) -- node[road, fill=green!20] {\(c_1\)} (C)
    (E) -- node[road, fill=green!20] {\(c_2\)} (F)
    (E) -- node[road, fill=green!20] {\(c_3\)} (D);
    \draw
    (F) -- node[road, fill=blue!20] {\(b_1\)} (B)
    (F) -- node[road, fill=blue!20] {\(b_2\)} (D);
    %
    \fill[black]
    (A) circle [radius=0.5mm]
    (B) circle [radius=0.5mm]
    (C) circle [radius=0.5mm]
    (D) circle [radius=0.5mm]
    (E) circle [radius=0.5mm]
    (F) circle [radius=0.5mm];
  \end{tikzpicture}
  %
  \caption{Example of Aggregator setting}
\end{figure}

% Majority voting
In our context, the majority voting aggregation function is used over the
received observations for a single road. The majority voting process is 
negating the incoming false information, knowing that only the
minority of the sensors can adopt an adversarial behavior

% Verification
Each PoW from the received observations needs to be verified by the aggregators.
The randomly generated number prevents the aggregators to add twice a single
observation.

% Certified Aggregates
Aggregators remove the PoW from the observation to lower down the load over the
network. So in order to keep the certification over the reported status, the
aggregators need to sign the aggregates.

\subsection{Miners}
% Algorithms
\begin{algorithm}[ht]
  \caption{The miner's procedure executed upon receiving an aggregate from an aggregator.}
  \SetKwBlock{Block}{}{}
  \SetKwFunction{Append}{Append}{}
  \SetKwArray{T}{T}
  Let $T$ be a set of transactions.\\
  \Block({\textbf{upon receiving} a transaction $t = (\sigma_a, pk_s, a)$ \textbf{from}
    \emph{an} aggregator}){
    \Begin(\emph{verify}){
      \tcp{check $pk_s$ in database}
      \tcp{check $a$ with $\sigma_a$ and $pk_s$}
    }
    \Begin(\emph{update}){
      $T \gets$ \Append{T,t}
    }
  }
\end{algorithm}

\begin{algorithm}[t]
  \caption{The miner's procedure executed to mine a block.}
  % 
  \SetKwBlock{Block}{}{}
  \SetKwHangingKw{Assert}{assert}
  \SetKwFunction{LatestBlock}{LatestBlock}
  \SetKwFunction{Hash}{Hash}
  \SetKwFunction{Sign}{Sign}
  \SetKwFunction{Append}{Append}
  \SetKwFunction{Majvote}{MajorityVoting}
  \DontPrintSemicolon%
  %
  Let \(T\) be the set of tansactions.\\
  Let \(pk_m\) the public key of the miner.\\
  Let \(sk_m\) the secret key of the miner.\\
  $previousBlock \gets $\LatestBlock{$chain$} \\
  $parent \gets $ \Hash{$previousBlock$} \\
  $h \gets ( parent, T, timestamp )$ \\
  $\sigma_h \gets $ \Sign{$sk_m, h$} \\
  $newBlock \gets ( \sigma_h, pk_m, h )$ \\
  \Return{\Append{$chain, newBlock$}} \\
\end{algorithm}

% Protocol
When a miner is elected by the PoS procedure at the commit of a block, he then
has to save every valid transactions created by the aggregators. After an 
arbitrary amount of time \(W\), the miner creates the block with all the saved
transactions. By creating the block, the current miner chooses at ranom the next miner who needs to 
generate the block to follow. The block is then broadcasted onto the network to 
the other miners and clients of the system.

% Adversarial Behavior
The miners can adopt an adversarial behavior in the system and are able to 
ignore unwanted transactions but are not capable of altering a certified 
transaction. The PoS method previously presented prevents a filling station 
from creating a sequence of blocks. Hence, an ignored transaction will
eventually be broadcasted with the following block and the system is then not 
fully aware of the totally of the observations for at most \(W\).

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% TeX-master: "main.tex"
%%% ispell-local-dictionary: "english"
%%% End:
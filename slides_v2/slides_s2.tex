\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{forest}
\usepackage{booktabs}

\usetheme{default}
\usecolortheme{lily}

% tight frame margins
\setbeamersize{text margin left=1em, text margin right=1em}

% figures are in figs/
\graphicspath{{figs/}}

% http://tex.stackexchange.com/a/68895
\usetikzlibrary{positioning, decorations.pathmorphing}
\tikzset{snake it/.style={decorate, decoration=snake}}

% tikz overlay, http://tex.stackexchange.com/a/16362
\tikzset{%
  every overlay node/.style={%
    %draw=black,fill=white,rounded corners,anchor=north west,
  },
}
\def\tikzoverlay{%
  \tikz[baseline,overlay]\node[every overlay node]
}

\usepackage[
style=ieee,
hyperref=true,
doi=false,
isbn=false,
url=false,
date=year,
backend=biber
]{biblatex}
\DeclareFieldFormat{pages}{}
\bibliography{../article_s2/main}

\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}

% hide nav symbols
\beamertemplatenavigationsymbolsempty%

% big frame numbers
\setbeamerfont{page number in head/foot}{size=\large}
\setbeamertemplate{footline}[frame number]

% no caption prefix
\captionsetup[figure]{labelformat=empty}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}

% footnote size
\setbeamerfont{footnote}{size=\tiny}

% no space after frametitle
\addtobeamertemplate{block begin}{\setlength\abovedisplayskip{0pt}}

\title{Permission-less and Distrubuted Framework for Traffic~Sensing by the Crowd}
\author{%
  Louis Béziaud\and
  Arthur Queffelec\\
  {\scriptsize under the supervision of}\\
  Emanuelle Anceaume\and
  Romaric Ludinard\\
  {\scriptsize with the participation of}\\
  Antonin Garret}
\institute{Université de Rennes 1 \& ENS Rennes\\
  {\tt \{first.last\}@etudiant.univ-rennes1.fr}}
\date{May 2017}

\begin{document}

\frame{\titlepage}

\begin{frame}{Traffic Monitoring}
  \begin{center}
    \includegraphics[width=0.7\linewidth]{traffic}
    
    \scriptsize{typical traffic in Rennes at 6PM on Fridays, according to Google Map}
  \end{center}
\end{frame}

\begin{frame}{Traffic Monitoring -- Data Collection}
  \begin{exampleblock}{Fixed Sensor Networks\only<1-2>{~\footfullcite{lang2001instantaneous}}}
    \only<1-2>{
      \begin{center}
        \includegraphics[width=0.6\linewidth]{cameras}

        \scriptsize{traffic light cameras in Providence, R.I. (\textit{The Providence Journal / Mary Murphy})}
      \end{center}%
    }%
    \only<2->{%
      high \alert{installation cost}, insufficient \alert{spatial coverage}, lack of \alert{scalability}
    }
  \end{exampleblock}
  \only<3->{
    \begin{block}{Mobile Crowd Sensing\only<3-4>{~\footfullcite{guo2015mobile}\footfullcite{wan2016mobile}}}
      \only<3>{
        \begin{center}
          \includegraphics[width=0.5\linewidth]{mcs}
        \end{center}%
      }%
      \only<4>{%
        \begin{minipage}[t]{.4\textwidth}\vspace{0pt} 
          \alert{no} installation \alert{cost} \\
          \alert{dynamic} spatial coverage \\
          \alert{scalable}
        \end{minipage}\hfill%
        \begin{minipage}[t]{0.6\textwidth}\vspace{0pt}
          \begin{center}
            \includegraphics[width=0.7\linewidth]{mcs}
          \end{center}
        \end{minipage}
      }
      \only<5->{
        \alert{no} installation \alert{cost}, \alert{dynamic} spatial coverage, \alert{scalable}
        
        \begin{center}
          \textbf{How to aggregate MCS data while keeping scalability?}
        \end{center}
        \only<6>{%
          \begin{center}
            \(\to\) \alert{blockchain}
          \end{center}
        }
      }
    \end{block}
  }
\end{frame}

\begin{frame}{Blockchain Overview}
  \begin{center}
    \includegraphics[width=0.7\linewidth]{btc_highlvl_workflow4}
  \end{center}
\end{frame}

\begin{frame}{Authenticity: Asymmetric Keys}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{asymmetric}
  \end{center}
\end{frame}

\begin{frame}{Ledger: Merkle Tree}
  \begin{center}
    \includegraphics[height=0.3\textheight]{mining}

    \scriptsize{merkle tree, from~\url{http://blog.csdn.net}}
    \vspace{1em}

    \includegraphics[height=0.3\textheight]{transactions}

    \scriptsize{from~\fullcite{NakamotoBitcoin2008}}
  \end{center}
\end{frame}

\begin{frame}{\(\sim\)Consensus: Proof of \underline{~~~~~}}
  \begin{block}{Proof of Work}
    pricing function~\footfullcite{DworkPricing1993}: \alert{expensive to produce}, \alert{cheap to verify}, non-amortizable
    
    \emph{e.g.}
    CAPTCHA~\footfullcite{von2003captcha} \includegraphics[height=\baselineskip]{captcha.png},
    Hashcash~\footfullcite{BackHashcash2002} hash inversion (\texttt{SHA256\^{}2})
  \end{block}

  \vspace{1em}
  
  \begin{block}{Proof of Stake}
    randomized block selection\quad \emph{e.g.} Nxt

    coin age based selection\quad \emph{e.g.} Peercoin
  \end{block}
\end{frame}

\begin{frame}
   \frametitle{Model}
	Identities\\
	\begin{itemize}
		\item Sensors
		\item Aggregators
		\item Miners
	\end{itemize}
	Infrastructure\\
	\begin{itemize}
		\item \alert{Trusted} GPS signal \alert{at all time}
		\item \alert{Nearly full} network coverage
	\end{itemize}
	
\end{frame}

\begin{frame}
	\frametitle{Security}
	\begin{itemize}
		\item \alert{Correct} entities
		\item \(> 50\%\) honest sensors \alert{uniformally distributed}
		\item Aggregators are \alert{trusted}
		\item Miners can adopt \alert{adversarial behaviour}
		\item Filing station \alert{collusion}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Overview}
	\begin{figure}
	\includegraphics[width=10cm]{overview.png}
	\caption{Overview of the framework}
	\end{figure}
\end{frame}

\begin{frame}
  	\frametitle{Sensor}
	\begin{itemize}
	\item Mobile nodes\\
  	\item Transmit observation\\
	\end{itemize}
	\[ o = (c, s, n, p) \]
	\only{
		\setbeamertemplate{items}[square]
  	\begin{itemize}
		\item \alert{Trusted} coordinates
   		\item \alert{Possibly wrong} observed state of the road
		\item Random number for \alert{unicity}
		\item PoW over \((c,s,n)\) with \alert{difficulty depends on road length}
  	\end{itemize}
	}
\end{frame}

\begin{frame}
	\frametitle{Aggregator}
	\begin{itemize}
	\item Geographically distributed\\
	\begin{figure}[ht]
	\centering
	  \begin{tikzpicture}[
    			aggr/.style={thick, rounded corners, line width=3mm, line cap=round},
    			road/.style={sloped, inner xsep=1pt, inner ysep=0pt, font=\small},
    			aggrlab/.style={minimum height=3mm, inner xsep=2pt, inner ysep=0pt, font=\small},
    				]
    		\coordinate (A) at (0,0);
		\coordinate (B) at (0,2);
    		\coordinate (C) at (2,0);
    		\coordinate (D) at (1,1);
    		\coordinate (E) at (3,1);
    		\coordinate (F) at (2,2);
    		%
    		\draw [aggr, draw=green!20]
    		(E.center) -- (F.center)
    		(E.center) -- (D.center)
    		(E.center) -- (C.center);
    		\draw [aggr, draw=blue!20]
    		(B.center) -- (F.center)
    		(D.center) -- (F.center);
    		\draw [aggr, draw=red!20]
    		(A.center) -- (B.center)
    		(A.center) -- (C.center)
    		(A.center) -- (D.center)
    		(C.center) -- (D.center);
    		%
    		\node[aggrlab, fill=red!20, xshift=-0.3mm]
    		at (A) [anchor=east] {aggregator \(a\)};
    		\node[aggrlab, fill=blue!20, xshift=0.3mm]
    		at (F) [anchor=west] {aggregator \(b\)};
    		\node[aggrlab, fill=green!20, xshift=0.3mm]
    		at (E) [anchor=west] {aggregator \(c\)};
    		%
    		\draw
    		(A) -- node[road, fill=red!20] {\(a_1\)} (B)
    		(A) -- node[road, fill=red!20] {\(a_2\)} (C)
    		(A) -- node[road, fill=red!20] {\(a_3\)} (D)
    		(C) -- node[road, fill=red!20] {\(a_4\)} (D);
    		\draw
    		(E) -- node[road, fill=green!20] {\(c_1\)} (C)
    		(E) -- node[road, fill=green!20] {\(c_2\)} (F)
    		(E) -- node[road, fill=green!20] {\(c_3\)} (D);
    		\draw
    		(F) -- node[road, fill=blue!20] {\(b_1\)} (B)
    		(F) -- node[road, fill=blue!20] {\(b_2\)} (D);
    		%
    		\fill[black]
    		(A) circle [radius=0.5mm]
    		(B) circle [radius=0.5mm]
    		(C) circle [radius=0.5mm]
    		(D) circle [radius=0.5mm]
    		(E) circle [radius=0.5mm]
    		(F) circle [radius=0.5mm];
  	  \end{tikzpicture}
	\end{figure}
	\item \alert{Removes load} over the network
	\item \alert{Lossy} and \alert{malicious-tolerant} method
	\item \alert{Keep certification} by signature
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Miner}
	\begin{itemize}
	\item \alert{Always accessible} nodes
	\item \alert{Elected} by Proof-of-Stake method
	\item \alert{Periodic generation} of block
	\item Corporation's filing station \alert{collusion}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Protocol}
	\begin{figure}
%msc{
%  # The entities
%   Sensors, Aggregator, ElectedMiner;
%
%   # Small gap before the boxes
%   |||;
%
%   # Next four on same line due to ','
%
%   # Example of the boxes with filled backgrounds
%   Sensors abox Aggregator [label="Observation", textbgcolour="#ff7f7f"];
%   Aggregator note Aggregator [label="Verification", textbgcolour="#7fff7f"];
%   Sensors abox Aggregator [label="Observation", textbgcolour="#ff7f7f"];
%   Aggregator note Aggregator [label="Verification", textbgcolour="#7fff7f"];
%   Sensors abox Aggregator [label="Observation", textbgcolour="#ff7f7f"];
%   Aggregator note Aggregator [label="Verification", textbgcolour="#7fff7f"];
%   Aggregator note Aggregator [label="Aggregation", textbgcolour="#7fff7f"];
%   Aggregator abox ElectedMiner [label="Transaction", textbgcolour="#7f7fff"];
%   Sensors abox Aggregator [label="Observation", textbgcolour="#ff7f7f"];
%   Aggregator note Aggregator [label="Verification", textbgcolour="#7fff7f"];
%   Aggregator note Aggregator [label="Aggregation", textbgcolour="#7fff7f"];
%   Aggregator abox ElectedMiner [label="Transaction", textbgcolour="#7f7fff"];
%   ElectedMiner note ElectedMiner [label="Verification", textbgcolour="#ffffff"];
%   ElectedMiner note ElectedMiner [label="Block", textbgcolour="#ffffff"];
%   
%}
	\includegraphics[width=6cm]{protocol.png}
	\caption{Example of a block generation}
	\end{figure}
\end{frame}

\begin{frame}{Conclusion}
  \begin{alertblock}{Done}
    \begin{itemize}
      \item proof-of-concept framework for mobile data aggregation
      \item based on the blockchain technology
      \item allows semi-adversarial behavior
      \item ledger for statistics
    \end{itemize}
  \end{alertblock}

  \begin{block}{Future Work}
    \begin{itemize}
      \item validation through simulation \& real-life experiment
      \item Can we eliminate the need for trusted aggregators?
      \item How does it translates to general crowd sensing?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \renewcommand*{\bibfont}{\scriptsize}
  \printbibliography
\end{frame}

\end{document}

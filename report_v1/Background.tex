\section{Background}\label{sec:Background}

In this section we first give a global view of the techniques used in road
planning. We then present the block chain data structure.

\subsection{Route planners}

\begin{figure}
\includegraphics[width=\linewidth]{roadplanner.pdf}
\caption{\lb{TODO}High-level view of our approach}
\label{fig:roadplanner}
\end{figure}

The route planning problem is to assign to each agent an optimal route from their
location to their destination, while avoiding collisions and deadlocks
involving other agents. Basic route planning generally models the problem as a
graph where the nodes represent geographic locations, such as junction, and
edges connect these locations (i.e.\ roads). Each edge is assigned a
weight (i.e.\ length). The optimization problem is then to
find the shortest paths between arbitrary source-target pairs. Finding an optimal
set of conflict-free route plans is an NP-hard
problem~\cite{GeisbergerAdvanced2011}. Many approximation algorithms have been
proposed to tackle the
problem~\cite{MorsContext2010,SchultesiRoute2008,SandersEngineering2007,GeisbergerAdvanced2011}.

In context-aware route planning, vehicles (agents) have to plan their route on
a common infrastructure in such a way that plans made by other agents are not
invalidated, and no conflicts are introduced. Agents place reservations on the
resources along their path, in such a way that the capacity of a resource is
never exceeded. Context awareness refers to the fact that an agent has to \aq{must} be
aware of the consequences of the route planned from other agents, since his
individually optimal route choice could be seriously affected by the route
choices of other agents. Given a set of reservations from previous agents that
may not be violated, finding an optimal route plan for a single agent can be
done with algorithms such as Dijkstra's~\cite{DijkstraNote1959}, or more
advanced methods~\cite{SandersEngineering2007}. This approach is similar to the
real-life scenario in which drivers look for road plans on websites, except
that these platforms do not ensure conflict freeness.

The quality of a computed route depends mainly on the data used to represent
the road network capacities. Current route planning systems use near real-time
data about road conditions such as traffic congestion, detours and traffic
collisions. This data can be obtained from
crowdsourcing~\cite{BarthBright2014,RoopaCrowdsourcing2013}\lb{+Google
Traffic}, GPS, WiFi, and mobile phone
network~\cite{ThiagarajanAccurate2009,HerreraEvaluation2010,MohanNericell2008},
or from cameras~\cite{BeymerComputer1997}, and radios. Pattern based
approaches are also developed to predict high load, using historical traffic
data under similar weather~\cite{HaighRoute1995}. Past traffic
patterns~\cite{HaighRoute1994,StaufferLearning2000} have also been used as data
to handle high and spontaneous loads.

Previous work on road reservation is the most related to our proposition. The idea is that each car on the road must have an assigned slot (ie.\ some space on the road). This system as been proposed as an alternative to current traffic lights~\cite{tachet2016revisiting} built upon slot-based systems similar to those commonly used in aerial traffic.

The idea of a global reservation system for cars as a way to avoid congestion as been first issued by~\cite{morla2005vision,iftode2008active}. \cite{cahill2008managed} has proposed a draft model along with research challenges. A complete system is proposed by~\cite{fang2012optimal}.\lb{TODO}

\subsection{Blockchain}

\begin{figure}
\includegraphics[width=\linewidth]{blockchainworkflow.pdf}
\caption{\lb{TODO}blockchain workflow}
\label{fig:blockchainworkflow}
\end{figure}

\ag{outil décentralisé (popriétés) acteur, temps de transfert variés
permitionless block chain vérifie props, nakamoto bitcoin} We are interested in
implementing a route planner on a distributed network because it offers several
properties that would go well with such a system. Besides the defining property
of sharing work and power of decision among peers, the fact that such a network
can be asynchronous fits our needs, because delays in communication will
certainly occur in a large system where all users may not always have reliable
access to the network (for example people traveling in the countryside). We
would also want our system to be permissionless, ie.\ for users to be able to
go in and out of the system at will, because that is exactly how real
automobile networks behave. That is why we will base our implementation on a
block chain protocol, a type of distributed algorithm that verifies these
wanted properties and was introduced by Nakamoto in 2008
\cite{NakamotoBitcoin2008} as the foundation of the now widely used Bitcoin
currency.

The purpose of a block chain is to allow the users of an asynchronous
distributed network to share common information, even when users may join or
quit the network at any moment without the need for permission.  A block chain
is a protocol in which each user possess a variable \textit{state} containing
\textit{blocks}. Blocks themselves contains a set of \textit{messages} and a
header pointing at the most recently created block before them. The blocks thus
form a chronologically ordered chain, hence the name ``block chain''.  
We want to achieve a \textit{public} ledger, ie.\
a common set of blocks that verifies the properties of persistency (once a
message gets added to the ledger, it is never removed) and liveliness (if
enough honest players want a message to be added to the ledger, it will
eventually be added).  Such a ledger should be strongly resistant to common attacks
such as sybil (when an attacker spawns lots of new players, in this case to control the 
messages added to the ledger) and allow each user to trust the data it contains. 
In 2016, Pass ~\cite{PassAnalysis2016} proved that a blockchain that possesses the properties
of \textbf{consistency} (the fact that all users share an identical 
chain of a growing number of blocks), \textbf{future self-consistence} (the fact that this 
common chain does not change as time goes on), \textbf{chain-growth} (the 
fact an honest player's chain will grow with as time goes on) and
\textbf{chain quality} (the fact that honest players actually get their messages added into 
the the chain) will allow for such a ledger.

He also proved that the Nakamoto protocol~\cite{NakamotoBitcoin2008} indeed possessed these properties. 
In this protocol, there is three types of nodes in the network 
: \textit{players}, who create messages and broadcasts them to the network; 
\textit{miners} who build blocks by collecting the messages they receive (once 
the block is mined, they in turn broadcast it to the network); 
\textit{users} who add the blocks they receive into the blockchain if and 
only if all the messages they contain are valid.

To ensure that at least one block is already shared by all nodes, the first block
of the chain, called genesis block, which purpose is solely to be said first
block, is directly encoded into the protocol, so that every node that uses it
will automatically accept the genesis block.  As for subsequent blocks, it is
definitely possible for two different blocks to be accepted by two different
users, thus creating a fork that \textit{de facto} transforms the chain into a
tree.  \ag{not rigorous but good for clarity? --> define block genesis, show
graphs}.

\begin{figure} \centering \begin{forest} [\(B_0\), fill={red!20}, for
tree={edge={<-},grow=east} [\(B_1\), for tree={fill=green!20} [\(B'_2\), for
tree={fill=gray!20}] [\(B_2\) [\(B_3\)] ] ] ] \end{forest} \caption{block chain
example (with a fork)}\label{fig:blockchain} \end{figure}

In order to preserve the unicity of the ledger shared by all users, a branch
must be chosen and one user must accept the other block and discard his own. To
solve this problem, a user will, upon receiving a block, check if it represents
a chain longer than the one represented by his last accepted block. If so, he
will discard this block and accept the new one. If not, he will discard the new
block. In the former case, if there exists messages that are present in the
discarded block but not in the newly accepted chain, he will add them in the
block he will start working on. This method allows for the consistency property
to be respected.  Now, one last issue is the need for users to add their block
into the block chain in a desynchronised fashion. Otherwise the forks created
by the addition of blocks from different users would never be solved.  This is
the goal of the proof-of-work scheme.

% TODO: POW (louis)

A proof-of-work~\cite{DworkPricing1993,JakobssonProofs1999} (POW) is a
computational puzzle used to deter denial of service
attacks~\cite{BorisovComputational2006} and other service abuse by requiring
some work from the service requester. It is a piece of data which is expensive
(costly, time-consuming) to produce but efficiently verifiable by others and
which satisfies certain requirements. It was first introduced as cost-functions
in~\cite{DworkPricing1993}.

Common proof-of-work implementations use a hash function as a building block,
as cryptographic hashes are hard to invert. This is the case of
Hashcash~\cite{BackHashcash2002}, used as the
Bitcoin~\cite{NakamotoBitcoin2008} mining function, which is using a double
SHA256 function. \lb{A full hash inversion has a computationally infeasible
brute-force running time, being \(O(2^k)\) where \(k\) is the hash size (eg.\
\(k=256\) for SHA256).}

The proof-of-work relies on energy use, since it requires high computation
power. Alternatives to POW have been proposed to mitigate this issue, such as
the proof-of-stake (POS). The main idea behind POS is that instead of using its
capital to buy computers and electricity to compute POW tokens, a user use its
capital to acquire the tokens. In cryptocurrencies such as
Peercoin~\cite{VasinBlackcoin2014}, a proof-of-stake asks users to prove
ownership of a certain amount of currency (their ``stake'' in the currency).

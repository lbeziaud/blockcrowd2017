# Permission-less and Distributed Framework for Traffic Sensing by the Crowd

Louis Béziaud, Antonin Garret and Arthur Queffelec, supervised by Emmanuelle Anceaume and Romaric Ludinard. 1st year master group project, 2017

see [report](report_v2/blockcrowd_report_v2.pdf) and [slides](slides_v2/blockcrowd_slides_v2.pdf)

## Abstract

Mobile crowd sensing is a large-scale sensing paradigm based on the power of user-companioned devices, which allows mobile users to share local knowledge acquired by their sensor-enhanced devices. The aggregation of this information provides large-scale sensing and community intelligence mining, replacing static sensing infrastructures. A centralized aggregation brings unavailability and censorship issues to community-driven services. We propose a framework for decentralized and open mobile crowd sensing based on the blockchain protocol.